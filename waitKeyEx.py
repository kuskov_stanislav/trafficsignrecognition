import cv2
import numpy as np

cv2.namedWindow('test')

while True:
    # key = cv2.waitKey(33) #this won't work
    # key = 0xFF & cv2.waitKey(33) #this is ok
    key = np.int16(cv2.waitKey(33))  # this is ok [2]

    if key == 27:
        break
    else:
        print key, hex(key), key % 256

cv2.destroyAllWindows()