

import cv2 as cv
import numpy as np

contour_area_smaller_countour = 1500
contour_area_smaller_color=300

def countour_detector(imgFull):
    sigma = 0.33

    v = np.median(imgFull)
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))

    img = cv.medianBlur(imgFull, 3)

    cimg =cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    imgCanny = cv.Canny(cimg, lower, upper)

    boxes_countour = list()
    contours, hierarchy = cv.findContours(imgCanny.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)


    for contour in contours:
        # parameters for optimization count of detectet objects
        contourArea = cv.contourArea(contour)
        x, y, w, h = cv.boundingRect(contour)


        moments = cv.moments(contour)  # Calculate moments
        if moments['m00'] != 0:
            c = contour
            rect = cv.minAreaRect(c)
            ##### make rotation for rect or for box!!!!!!!
            box = np.int0(cv.cv.BoxPoints(rect))

            if not (box is None):
                if contourArea>contour_area_smaller_countour and w/h>0.3 and w/h<3:
                    boxes_countour.append(box)
                    cv.drawContours(imgFull, [box], -1, (0, 255, 0), 3)  # draw contours in green color
    return boxes_countour

def color_detector_red(imgFull):

    hsv = cv.cvtColor(imgFull, cv.COLOR_BGR2HSV)

    hsv = cv.blur(hsv, (5, 5))

    lower_red = np.array([85, 60, 110])  # camera web red
    upper_red = np.array([255, 255, 255])

    thresh_red = cv.inRange(hsv, lower_red, upper_red)
    thresh_red = cv.erode(thresh_red, None, iterations=1)  # delete other white pixels
    thresh_red = cv.dilate(thresh_red, None, iterations=1)  # not change ROI blob size in prev function (erode)

    boxes_color_red=list()
    contours_red, hierarchy_red = cv.findContours(thresh_red.copy(), cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

    for contour_red in contours_red:
        # parameters for optimization count of detectet objects
        contourArea = cv.contourArea(contour_red)
        x, y, w, h = cv.boundingRect(contour_red)
        moments = cv.moments(contour_red)  # Calculate moments
        if moments['m00'] != 0:
            c = contour_red
            rect = cv.minAreaRect(c)
            ##### make rotation for rect or for box!!!!!!!
            box = np.int0(cv.cv.BoxPoints(rect))

            if not (box is None):
                if contourArea > contour_area_smaller_color and w / h > 0.3 and w / h < 3:
                    boxes_color_red.append(box)
                    cv.drawContours(imgFull, [box], -1, (255, 255, 255), 3)  # draw contours in green color
    return boxes_color_red
    # cv.imshow('thresh_red', thresh_red)

def color_detector_blue(imgFull):
    # cv.imshow('input', self.videoCapture)
    hsv = cv.cvtColor(imgFull, cv.COLOR_BGR2HSV)

    hsv = cv.blur(hsv, (5, 5))

    lower_blue = np.array([70, 110, 70])  # camera web blue
    upper_blue = np.array([132, 255, 255])


    thresh_blue = cv.inRange(hsv, lower_blue, upper_blue)
    thresh_blue = cv.erode(thresh_blue, None, iterations=2)  # delete other white pixels
    thresh_blue = cv.dilate(thresh_blue, None, iterations=12)  # not change ROI blob size in prev function (erode)

    boxes_color_blue=list()
    contours, hierarchy = cv.findContours(thresh_blue.copy(), cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

    for contour in contours:
        # parameters for optimization count of detectet objects
        contourArea = cv.contourArea(contour)
        x, y, w, h = cv.boundingRect(contour)
        moments = cv.moments(contour)  # Calculate moments
        if moments['m00'] != 0:
            c = contour
            rect = cv.minAreaRect(c)
            ##### make rotation for rect or for box!!!!!!!
            box = np.int0(cv.cv.BoxPoints(rect))

            if not (box is None):
                if contourArea > contour_area_smaller_color and w / h > 0.3 and w / h < 3:
                    boxes_color_blue.append(box)
                    cv.drawContours(imgFull, [box], -1, (0, 255, 255), 3)  # draw contours in green color

    return boxes_color_blue

stream = cv.VideoCapture(0)  # camera number

width = 640  # camera capture width/height
height = 480

widthFr = 40  # signs size
heightFr = 40

stream.set(3, width)
stream.set(4, height)

if stream.isOpened() == False:
    print "Cannot open input video"
    exit()

frameNumber = 0

while (cv.waitKey(1) != 27):
    frameNumber += 1
    # print datetime.datetime.now()
    flag, imgFull = stream.read()

    if flag == False: break  # end of video

    # traffic signs places as i think
    boxes_color_blue=color_detector_blue(imgFull)
    boxes_color_red=color_detector_red(imgFull)
    boxes_countour=countour_detector(imgFull)

    cv.imshow('imgFull', imgFull)


cv.destroyAllWindows()