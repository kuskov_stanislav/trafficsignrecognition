# y key - detected sign on frame
# n key - not detected sign on frame


import numpy as np
import cv2 as cv
from signDetection import signDetection as SD
from signRecognitionCNN import signRecognitionCNN

class Tests:
    video_dir="tests/videos/"
    video_name="MOV_0051.mp4"

    def video_test(self):
        video_dir=self.video_dir
        video_name=self.video_name
        # stream = cv.VideoCapture(0)  # camera number
        detection = SD.signDetectorOpenCV()

        stream = cv.VideoCapture(video_dir+video_name)

        width = 640     #camera capture width/height
        height = 480

        widthFr = 40    #signs size
        heightFr = 40

        stream.set(3, width)
        stream.set(4, height)

        predict = signRecognitionCNN.imgRecognitionCNN()

        if stream.isOpened() == False:
            print "Cannot open input video"
            exit()

        frameNumber = 0
        z,x,c,v = 0,0,0,0
        while (cv.waitKey(1) != 27):
            key = cv.waitKey(0) % 0x100


            if key == ord('z'):
                z+=1
                print ("z= "+str(z))
            if key == ord('x'):
                x +=1
                print ("x= "+str(x))
            if key == ord('c'):
                c +=1
                print ("c= "+str(c))
            if key == ord('v'):
                v +=1
                print ("v= "+str(v))
            frameNumber += 1
            # print datetime.datetime.now()
            flag, imgFull = stream.read()
            #cv.resize(imgFull,(640,480), interpolation = cv.INTER_CUBIC)
            # print imgFull
            if flag == False: break  # end of video

            detection.videoCapture = imgFull  # detecting signs
            imgGeneralProcessing = detection.videoCaptureGeneralProcesing()
            signRoi = detection.imgSingDetecting(imgFull, imgGeneralProcessing, widthFr, heightFr)

            cv.imshow('imgFull', imgFull)

            if not (signRoi is None):
                cv.imshow('signRoi', signRoi)
                signRoi = signRecognitionCNN.teX(signRoi)  # recognition signs
                signValue = predict(signRoi)
                print signRecognitionCNN.signName(int(signValue))


        cv.destroyAllWindows()
