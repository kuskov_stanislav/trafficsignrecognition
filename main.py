from signDetection import signDetection as SD
import cv2 as cv
from signRecognitionCNN import signRecognitionCNN
import datetime
import tests.tests as tests


def main():
    detection = SD.signDetectorOpenCV()


    stream = cv.VideoCapture(0) # camera number

    width = 640     #camera capture width/height
    height = 480


    widthFr = 40    #signs size
    heightFr = 40

    stream.set(3, width)
    stream.set(4, height)
    predict=signRecognitionCNN.imgRecognitionCNN()

    if stream.isOpened() == False:
        print "Cannot open input video"
        exit()


    frameNumber = 0

    while (cv.waitKey(1) != 27):
        frameNumber += 1
        #print datetime.datetime.now()
        flag, imgFull = stream.read()

        if flag == False: break  # end of video

        detection.videoCapture=imgFull #detecting signs

        imgGeneralProcessing=detection.videoCaptureGeneralProcesing()
        # print datetime.datetime.now()
        signRoi=detection.imgSingDetecting(imgFull,imgGeneralProcessing,widthFr,heightFr)


        # print datetime.datetime.now()

        if not(signRoi is None):

            cv.imshow('output', signRoi)
            signRoi=signRecognitionCNN.teX(signRoi)     #recognition signs
            signValue = predict(signRoi)
            print signRecognitionCNN.signName(int(signValue))


        else:
            cv.destroyWindow('output')
        print datetime.datetime.now()

        cv.imshow('imgFull', imgFull)
    cv.destroyAllWindows()

def main_detector():
    detection = SD.signDetectorOpenCV()

    stream = cv.VideoCapture(2)  # camera number

    width = 640  # camera capture width/height
    height = 480

    widthFr = 40  # signs size
    heightFr = 40

    stream.set(3, width)
    stream.set(4, height)

    # predict = signRecognitionCNN.imgRecognitionCNN()

    if stream.isOpened() == False:
        print "Cannot open input video"
        exit()

    frameNumber = 0

    while (cv.waitKey(1) != 27):
        frameNumber += 1
        # print datetime.datetime.now()
        flag, imgFull = stream.read()

        if flag == False: break  # end of video
        detection.videoCapture = imgFull  # detecting signs

        imgGeneralProcessing = detection.videoCaptureGeneralProcesing()
        # print datetime.datetime.now()
        signRoi = detection.imgSingDetecting(imgFull, imgGeneralProcessing, widthFr, heightFr)
        # signRoi = detection.imgSingDetectingAlgorithmic(imgFull, imgGeneralProcessing, widthFr, heightFr)
        # refractoring detection:

        # print datetime.datetime.now()

        if not (signRoi is None):
            cv.imshow('output', signRoi)
            # signRoi = signRecognitionCNN.teX(signRoi)  # recognition signs
            # signValue = predict(signRoi)
            # print signRecognitionCNN.signName(int(signValue))


        else:
            cv.destroyWindow('output')

        cv.imshow('imgFull', imgFull)
    cv.destroyAllWindows()

if __name__ == '__main__':
    #TestClass=tests.Tests()
    #TestClass.video_test()

    # main()

    main_detector()

# THEANO_FLAGS=mode=FAST_RUN,device=gpu0,floatX=float32,optimizer=fast_compile python main.py

