# -*- coding: utf-8 -*-

# выделение знаков на исходных изображениях, сохранение выделенных знаков в новый каталог
import numpy as np
import  cv2 as cv
from os import listdir
import os
import csv

class datasetCreator:
    arg = 'Python' # Все экземпляры этого класса будут иметь атрибут arg,
                   # равный "Python"
                   # Но впоследствии мы его можем изменить
    widthFr = 100
    heightFr = 100

    def singMaker(self):        #sign creating: originsla img - cut roi - make image on roi

        type = listdir("fullsigns")
        for valType in type:
            signs=listdir("fullsigns/"+valType)

            for valSigns in signs:

                print valSigns

                signsDir="fullsigns/"+valType+"/"+valSigns
                signs = DC.imgOpen(signsDir)

                procSigns, imgFullResize = DC.imgGeneralProcessing(signs)
                cv.imshow('procSigns', procSigns)

                singDetection = DC.imgSingDetect(imgFullResize, procSigns)

                if singDetection is None:
                    cv.destroyWindow('output')
                    cv.destroyWindow('outputRec')
                else:
                    cv.imshow('output', singDetection)

                    directory = 'signs/'+valType
                    if not os.path.exists(directory):
                        os.makedirs(directory)
                    # key = cv.waitKey() % 256
                    # if key == 10:
                    cv.imwrite(directory + '/'+valSigns, singDetection)
                        # print cv.waitKey() % 256




    def imgOpen(self,signsDir):
        signs = cv.imread(signsDir)
        return signs


    def imgGeneralProcessing(self, imgFull):                    # image processing for detect sign roi
        imgFullResize=cv.resize(imgFull, (int(1024), int(960)))
        cv.imshow('imgFullResize', imgFullResize)
        hsv = cv.cvtColor(imgFullResize, cv.COLOR_BGR2HSV)
        hsv2 = cv.blur(hsv, (1, 1))

        # lower_blue = np.array([140,126,127])            # camera web best
        # upper_blue = np.array([255,255,255])  #Red

        # lower_blue = np.array([45,179,211])            # camera web best
        # upper_blue = np.array([255,255,255])  #Blue

        lower_blue = np.array([78,34,128])            # camera web best
        upper_blue = np.array([255,255,255])

        # lower_blue = np.array([20, 150, 80])  # camera web best
        # upper_blue = np.array([200, 255, 180])

        threshBlue = cv.inRange(hsv2, lower_blue, upper_blue)
        threshBlue = cv.erode(threshBlue, None, iterations=2)  # delete other white pixels
        threshBlue = cv.dilate(threshBlue, None, iterations=6)  # not change ROI blob size in prev function (erode)

        return threshBlue,imgFullResize

    def imgSingDetect(self,imgFull, imgGP):     # roi detecting on image

        contours, hierarchy = cv.findContours(imgGP.copy(), cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

        for cnt in contours:
            moments = cv.moments(cnt)  # Calculate moments
            if moments['m00'] != 0:
                c = sorted(contours, key=cv.contourArea, reverse=True)[0]
                rect = cv.minAreaRect(c)
                ##### make rotation for rect or for box!!!!!!!
                box = np.int0(cv.cv.BoxPoints(rect))
                print rect


                if box.any:

                    y1 = int(box[0][1])
                    x2 = int(box[1][0])
                    y2 = int(box[1][1])
                    x3 = int(box[2][0])
                    roi = imgFull[y2:y1, x2:x3]
                    roiImg = roi.copy()

                    cv.drawContours(imgFull, [box], -1, (0, 255, 0), 3)  # draw contours in green color
                    cv.imshow('input', imgFull)
                    if not roiImg.any():
                        print ""
                    else:
                        newimg = cv.resize(roiImg, (int(self.widthFr), int(self.heightFr)))
                        RECnewimg = newimg.copy()
                        return RECnewimg

class csvMaker:
    #
    widthFr = 40
    heightFr = 40
    signPath='csvsigns/'
    arraySigns=np.array([])
    xTrain = np.array([])
    xTest = np.array([])

    yTrain=np.array([])
    yTest = np.array([])

    signsCount = len(listdir(signPath)) # 1 - nosign examples (path 0)

    def makeCsv(self):
        type=listdir(self.signPath)

        ySet=[]

        for valType in type:

            print valType

            signs=listdir(self.signPath+valType)



            lenSigns=len(signs)

            trainLength = int(float(lenSigns) / 100 * 70)

            ySet = np.zeros(int(self.signsCount))

            # if int(valType) != 0:
            #     ySet[int(valType) - 1] = 1
            ySet[int(valType)] = 1

            arraySignsPart=np.array([])

            for valSigns in signs:

                pixelArray = cv.imread(self.signPath + valType + '/' + valSigns)

                pixelArray = cv.resize(pixelArray, (int(self.widthFr), int(self.heightFr)))

                pixelArray = cv.cvtColor(pixelArray,cv.COLOR_BGR2GRAY)

                pixelArrayList=[]

                for x in pixelArray:
                    for y in x:
                        pixelArrayList.append(y)


                if arraySignsPart.any():
                    arraySignsPart = np.append(arraySignsPart, [pixelArrayList], axis=0)
                else:
                    arraySignsPart=np.array([pixelArrayList])


                if self.arraySigns.any():
                    self.arraySigns = np.append(self.arraySigns, [pixelArrayList], axis=0)
                else:
                    self.arraySigns=np.array([pixelArrayList])




            xTrainPart = arraySignsPart[:trainLength]
            xTestPart = arraySignsPart[trainLength:]


            for xTrainLine in xTrainPart:

                if self.xTrain.any():
                    self.xTrain = np.append(self.xTrain, [xTrainLine], axis=0)
                    self.yTrain = np.append(self.yTrain, [ySet], axis=0)
                else:
                    self.xTrain = np.array([xTrainLine])
                    self.yTrain = np.array([ySet])

            for xTestLine in xTestPart:

                if self.xTest.any():
                    self.xTest = np.append(self.xTest, [xTestLine], axis=0)
                    self.yTest = np.append(self.yTest, [ySet], axis=0)
                else:
                    self.xTest = np.array([xTestLine])
                    self.yTest = np.array([ySet])

        # print len(self.xTrain),len(self.xTest),len(self.yTrain),len(self.yTest)
        np.savetxt('xTrain.csv', self.xTrain, fmt='%.18g', delimiter=',', newline=os.linesep)
        np.savetxt('yTrain.csv', self.yTrain, fmt='%.18g', delimiter=',', newline=os.linesep)

        np.savetxt('xTest.csv', self.xTest, fmt='%.18g', delimiter=',', newline=os.linesep)
        np.savetxt('yTest.csv', self.yTest, fmt='%.18g', delimiter=',', newline=os.linesep)


    # classes for using
DC = datasetCreator()
CM = csvMaker()


# using

CM.makeCsv()

# DC.singMaker()
