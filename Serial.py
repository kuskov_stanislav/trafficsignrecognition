# -*- coding: utf-8 -*-
# Включить:
# красная кнопка на руле -> желтая кнопка (держать 9 сек)
# посылка из 2-х быйт, первый - направление, второй - скорость
# \x31\xFF - вперед (второй байт от 5A до FF)
# \x32\xFF - Назад (второй байт от 6A до FF)
#
# \x31\xFF -> \x31\x00 -> \x32\x00 -> \x32\xFF
#
# использовать для движения вперед -> назад:
#
# \x33\x00 - отпустить тормоз
# \x34\x00 - нажать тормоз
# \x54\x00 - Опрос АЦП

# \x64\x03\xFF - поворот колес (2 и 3 байты от x0040 до x0390, центр - x0200)
# Использовать для поворота значения:
#     крайнее левое - x0040
#     крайнее правое - x0390
#     центр - x0200

# Соответствие кодов:
# движение вперед - 0 -255
# Повороты - 64 - 912

import serial

import time

ser = serial.Serial(
    port='/dev/ttyUSB0',
    baudrate=9600,
    dsrdtr=1,
    timeout=0,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS
)

time.sleep(1)

testSter = "\x64\x02\x00"
testSpeed = "\x31\x02"
ser.write(testSter)
ser.close()